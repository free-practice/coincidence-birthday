"""Имитационное моделирование вероятности совпадений дней рождений."""
import datetime
import random
import sys


def main():
    # Запрашиваем выбор теста у пользователя
    print('1. Вероятность совпадения одного дня рождения в n-группе людей\n'
          '2. Среднее количество человек нужных для совпадения n-числа дней рождений\n'
          '3. Среднее количество случайных дней нужных для составления n-разных дат\n'
          'q. Выход (в любой момент запроса ввода)'
          )
    var = ask_number(1, 4, 'Введите число интересующего теста или "q" для выхода: ')

    # Запрашиваем количество значений для теста у пользователя
    select = {1: (2, 87, 'Введите количество человек в группе (от 2 до 86): ', ),
              2: (1, 366, 'Введите количество необходимых совпадений дней рождений (от 1 до 365): ',),
              3: (2, 366, 'Введите количество необходимых разных дат (от 2 до 365): ', ),
              }
    quantity = ask_number(*select[var])

    # Запрашиваем количество тестов у пользователя
    test_count = ask_number(1000, 100_001, 'Введите число тестов (от 1000 до 100_000): ')

    # Цикл подсчёта результата и вывода полосы загрузки
    attempt = 0
    results = []
    percent = 1
    func = {1: get_any_coincidence, 2: collect_all_coincidences, 3: collect_all_days, }[var]
    print('____________________________________________________________________________________________________\n'
          '|_____10%|______20%|______30%|______40%|______50%|______60%|______70%|______80%|______90%|_____100%|')
    while attempt < test_count:
        attempt += 1
        if attempt >= test_count / 100 * percent:
            percent += 1
            print('|', end='')
        results.append(func(quantity))

    # Вывод результатов
    amount = round(sum(results) / test_count * (100 if var == 1 else 1), 2)
    print(f'\nПроведено моделирований: {test_count:_d}')
    match var:
        case 1:
            print(f'Вероятность совпадения хотя бы одного дня рождения в группе из {quantity} человек: {amount}%')
        case 2:
            print(f'Среднее количество человек необходимое для совпадения {quantity} дней рождений: {amount}')
        case 3:
            print(f'Среднее количество случайных дней необходимых для сбора {quantity} разных дат: {amount}')
    input('Нажмите Enter чтобы продолжить')


def ask_number(start, stop, text='Введите значение: '):
    """
    Возвращает число, запрошенное у пользователя или завершает программу при вводе 'q'
    Обязательные аргументы: start и stop(в какой диапазон включительно должно входить число)
    Необязательный аргумент: text(выводит текст при запросе на ввод)
    """
    while True:
        number = input('\n'+text)
        if number.lower() == 'q':
            sys.exit()
        if number.isdigit() and start <= int(number) < stop:
            return int(number)


def get_random_days(peoples):
    """Возвращает список случайных дней в невисокосном году"""
    birthdays = []
    for _ in range(peoples):
        start_day = datetime.date(2001, 1, 1)
        day = datetime.timedelta(random.randint(0, 364))
        birthdate = str(start_day + day).split('-')
        birthday = f'{birthdate[2]}.{birthdate[1]}'
        birthdays.append(birthday)
    return birthdays


def get_any_coincidence(quantity):
    """Возвращает 1 если совпал хотя бы один день из quantity-числа дней, иначе 0 (var == 1)"""
    birthdays = get_random_days(quantity)
    if len(birthdays) == len(set(birthdays)):
        return 0
    else:
        return 1


def collect_all_coincidences(quantity):
    """Возвращает количество попыток до quantity-числа совпадений дней рождений (var == 2)"""
    days = set()
    count = 0
    count_same = 0
    while count_same < quantity:
        day = get_random_days(1)[0]
        count += 1
        if day in days:
            count_same += 1
        days.add(day)
    return count


def collect_all_days(quantity):
    """Возвращает количество попыток за сколько соберётся quantity-число разных дат из случайных дней (var == 3)"""
    year = set()
    count = 0
    while len(year) < quantity:
        year.add(*get_random_days(1))
        count += 1
    return count


if __name__ == '__main__':
    while True:
        main()
